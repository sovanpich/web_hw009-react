import 'bootstrap/dist/css/bootstrap.min.css';
import MenuBar from './Components/MenuBar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from './Components/Home';
import Video from './Components/Video';
import Account from './Components/Account';
import Welcome from './Components/Welcome';
import Auth from './Components/Auth';
import Detail1 from './Pages/Detail1';
import Movies from './Pages/Movies';

function App() {
 
  return (
    <Router>
      <MenuBar />
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/Video"  component={Video} />
        <Route path="/Account"  component={Account} />
        <Route path="/Welcome"  component={Welcome} />
        <Route path="/Auth"  component={Auth} />
        <Route path="/detail/:id"  component={Detail1} />
      </Switch>
    </Router>
  );
}

export default App;
