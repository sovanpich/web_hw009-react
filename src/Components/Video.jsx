import React from 'react'
import { Container, Button, Row } from 'react-bootstrap'
import { BrowserRouter as Router, Switch, Route, Link ,useRouteMatch} from 'react-router-dom'
import Movies from '../Pages/Movies'

function Video() {

    let { path, url } = useRouteMatch();

    return (
        <Container>
            <h2>Topics</h2>
            <div>
                <Button variant="primary" as={Link} to={`${url}/Movies Category`}>Movies</Button>
                <Button variant="primary" as={Link} to={`${url}/Animation Category`}>Animation</Button>
            </div>

            <Switch>
                <Route exact path={path}>
                </Route>
                <Route path={`${path}/:topicId`}>
                    <Movies />
                </Route>
            </Switch>
        </Container>
    );


}

export default Video
