import React from 'react'
import { Container,Nav,Navbar,Button,Form,FormControl } from 'react-bootstrap'
import {Link} from 'react-router-dom'


function MenuBar() {
    return (
        
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand href="#home">React-Routing</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link as={Link} to="/">Home</Nav.Link>
                            <Nav.Link as={Link} to="/Video">Video</Nav.Link>
                            <Nav.Link as={Link} to="/Account">Account</Nav.Link>
                            <Nav.Link as={Link} to="/Auth">Welcome</Nav.Link>
                            <Nav.Link as={Link} to="/Auth">Auth</Nav.Link>
                        </Nav>
                        <Nav>
                            <Form className="d-flex">
                                <FormControl
                                    type="search"
                                    placeholder="Search"
                                    className="mr-2"
                                    aria-label="Search"
                                />
                                <Button variant="outline-success">Search</Button>
                            </Form>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        
    )
}

export default MenuBar
