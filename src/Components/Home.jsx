import React from 'react'
import { Container, Row, Col, Card, Button } from 'react-bootstrap'
import { useState } from 'react'
import { Link } from 'react-router-dom'

function Home() {
    const [data, setData] = useState([
        {
            id: 1,
            name: 'Hidden Figures',
            content: 'Hidden Figures is a 2016 American biographical drama film directed by Theodore Melfi and written by Melfi and Allison Schroeder.',
            url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmrYd56tZSinmEgaAG7VxsYwDOCNjvvnzdvw&usqp=CAU',
            detail: "/detail/1"
        },
        {
            id: 2,
            name: 'The Blind Side',
            content: 'The Blind Side is a 2009 American biographical sports drama film written and directed by John Lee Hancock. Based on the 2006 book The Blind Side: Evolution of a Game by Michael Lewis,[2][3] the film tells the story of Michael Oher, an American football offensive lineman   ',
            url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS52idNP52Gm-bgMxdRYXS7PxI09lz6EzGtbBq-sgtC9OnxzZzHijCprp6IIbNprD4Nx04&usqp=CAU',
            detail: "/detail/2"
        },
        {
            id: 3,
            name: 'Gifted',
            content: 'Gifted is a 2017 American drama film directed by Marc Webb and written by Tom Flynn. It stars Chris Evans',
            url: 'https://ui.assets-asda.com/dm/asdagroceries/5039036081825_T1?defaultImage=asdagroceries/noImage&resMode=sharp2&id=EMFSF1&fmt=jpg&fit=constrain,1&wid=256&hei=256',
            detail: "/detail/3"
        },
        {
            id: 4,
            name: 'The Good Doctor',
            content: 'The Good Doctor is an American medical drama television series based on the 2013 South Korean series of the same name. Actor Daniel Dae Kim noticed the original series and bought the rights for his production company. He began adapting the series and, in 2015, eventually shopped it to CBS,',
            url: 'https://upload.wikimedia.org/wikipedia/en/6/62/The_Good_Doctor_2017.png',
            detail: "/detail/4"
        },
        {
            id: 5,
            name: 'X+Y',
            content: 'Struggling to build relationships with others, a teenage math prodigy (Asa Butterfield) develops a budding friendship with a young girl (Jo Yang) while competing at the International Mathematics Olympiad.A young mathematical genius played by Asa Butterfield struggles with his autism and the aftermath of his father’s death',
            url: 'https://www.telegraph.co.uk/content/dam/film/X%20%2B%20Y/x-plus-y.jpg?imwidth=450',
            detail: "/detail/5"
        },
        {
            id: 6,
            name: 'The Man Who Knew Infinity',
            content: 'This film features mathematician Srinivasa Ramanujan’s journey in Cambridge to become a theoretical mathematician. There, he faced racism and health problems that',
            url: 'https://d29pz51ispcyrv.cloudfront.net/images/I/9KuauAEALRnrgMR1P.MD256.JPEG',
            detail: "/detail/6"
        },
        {
            id: 7,
            name: 'Good Will Hunting',
            content: 'Good Will Hunting is another inspiring movie for underdogs. In this drama film, Matt Damon stars as Will Hunting, a janitor who also happens to be a genius at math',
            url: 'https://iconarchive.com/download/i100627/firstline1/movie-mega-pack-1/Good-Will-Hunting.ico',
            detail: "/detail/7"
        },
        {
            id: 8,
            name: 'Hidden Figures',
            content: 'This Thai heist thriller tells the story of two brilliant high school students who start a cheating business. Lynn and Bank, top students in their schools, plan to help their classmates cheat the STIC in exchange for money. These two are so awesome, even I would want them to help write my college essays.A genius level high school student makes money after developing elaborate methods to help other students cheat.',
            url: 'https://i0.wp.com/www.backseatmafia.com/wp-content/uploads/2017/09/Bad-Genius.jpg?fit=700%2C259&ssl=1&w=640',
            detail: "/detail/8"
        }
    ])
    
    return (
        <Container>
            <Row>
                {
                    data.map((value, id) => {
                        return (

                            <Col xs key={id}>
                                <Card style={{ width: '18rem' }}>
                                    <Card.Img variant="top" src={value.url} />
                                    <Card.Body>
                                        <Card.Title>{value.name}</Card.Title>
                                        <Card.Text>{value.content}</Card.Text>
                                        <Button variant="primary" as={Link} to={value.detail}>Detail</Button>
                                    </Card.Body>
                                </Card>
                            </Col>


                        )
                    })

                }
            </Row>
        </Container>
    )
}

export default Home
