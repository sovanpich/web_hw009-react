import React from 'react'
import {Button} from 'react-bootstrap'

function Welcome() {
    return (
        <div>
           <h1>Welcome</h1>
           <Button variant="primary" >Log Out</Button>
        </div>
    )
}

export default Welcome
