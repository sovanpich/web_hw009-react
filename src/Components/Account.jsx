import React from 'react'
import { Container } from 'react-bootstrap'
import { useState } from 'react'
import { Link,useRouteMatch,Switch,Route } from 'react-router-dom'
import AccountDetail from '../Pages/AccountDetail';

function Account() {
    
    let { path, url } = useRouteMatch();
    return (
        <Container>
            <h1>Account</h1>
            <ul>
                <li>
                    <Link to={`${url}/Netflix`}>
                        Netflix
                    </Link>
                </li>
                <li>
                    <Link to={`${url}/Zillow Group`}>
                       Zillow Group
                    </Link>
                </li>
                <li>
                    <Link to={`${url}/Yahoo`}>
                        Yahoo
                    </Link>
                </li>
                <li>
                    <Link to={`${url}/Modus Create`}>
                        Modus Create
                    </Link>
                </li>
            </ul>
            <Switch>
                <Route exact path={path}>
                </Route>
                <Route path={`${path}/:accountName`}>
                    <AccountDetail/>
                </Route>
            </Switch>
        </Container>
    )
}

export default Account
