import React from 'react'
import { Button, Row, col } from 'react-bootstrap'
import { useParams, Link, useRouteMatch,Switch,Route,Router } from 'react-router-dom'
import CategoryVideo from './CategoryVideo';

function Movies() {
    let { path, url } = useRouteMatch();
    let { topicId } = useParams();
    function condition() {
        if (topicId =='Movies Category') {
            return (
                <div>
                    <Button variant="primary" as={Link} to={`${url}/Adventure`}>Adventure</Button>
                    <Button variant="primary" as={Link} to={`${url}/Crime`}>Crime</Button>
                    <Button variant="primary" as={Link} to={`${url}/Action`}>Action</Button>
                    <Button variant="primary" as={Link} to={`${url}/Romance`}>Romance</Button>
                    <Button variant="primary" as={Link} to={`${url}/Comedy`}>Comedy</Button>
                </div>
            )
        } else {
            return (
                <div>
                    <Button variant="primary" as={Link} to={`${url}/Action`}>Action</Button>
                    <Button variant="primary" as={Link} to={`${url}/Romance`}>Romance</Button>
                    <Button variant="primary" as={Link} to={`${url}/Comedy`}>Comedy</Button>
                </div>
            )
        }

    }
    return (
        <div>
            <h3>{topicId}</h3>
            <hr />
            <div>{condition()}</div>
            <h2>Please Choose Category</h2>
            <Switch>
                <Route exact path={path}>
                </Route>
                <Route path={`${path}/:topicId`}>
                    <CategoryVideo/>
                </Route>
            </Switch>
        </div>
    );
}

export default Movies
