import React from 'react'
import { useParams } from 'react-router-dom'
function AccountDetail() {
    let { accountName } = useParams();
    return (
        <div>
            <h2>ID : {accountName}</h2>
        </div>
    )
}

export default AccountDetail
