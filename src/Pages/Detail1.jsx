import React from 'react'
import {useParams} from 'react-router-dom'

function Detail1() {
    let {id} = useParams()
    return (
        <div>
            <h3>Detail{id} </h3>
        </div>
    )
}

export default Detail1
